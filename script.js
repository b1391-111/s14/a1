console.log("Hello World");
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let address = {
    houseNumber: 32,
    street: 'Washington',
    city: 'Lincoln',
    state: 'Nebraska'
};


function printUserInfo(firstName, lastName, age) {
    let userInfo = `${firstName} ${lastName} is ${age} years of age`;

    console.log(`First Name: ${firstName}`)
    console.log(`Last Name: ${lastName}`)
    console.log(`Age: ${age}`)
    console.log(hobbies);
    console.log(address);

    console.log(userInfo);
    console.log(`This was printed inside printUserInfo function:`);
    console.log(hobbies);
    console.log(`This was printed inside printUserInfo function:`);
    console.log(address);
}

function storeVar(married) {
    let isMarried = married;
    return isMarried;
}

printUserInfo("John", "Smith", 30);
console.log(`The value of isMarried is ${storeVar(true)}`);